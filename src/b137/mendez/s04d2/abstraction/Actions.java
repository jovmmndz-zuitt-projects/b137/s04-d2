package b137.mendez.s04d2.abstraction;

public interface Actions {
    default void sleep() {
        System.out.println("Zzz...");
    };
    default void run() {
        System.out.println("Running...");
    };
}
