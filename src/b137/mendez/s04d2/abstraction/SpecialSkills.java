package b137.mendez.s04d2.abstraction;

public interface SpecialSkills {
    default void computerProgram() {
        // Implementation
        System.out.println("I can program in Java!");
    }
    default void driveACar() {
        System.out.println("I can drive a car!");
    };
    //public void driveACar();
}
