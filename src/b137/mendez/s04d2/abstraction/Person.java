package b137.mendez.s04d2.abstraction;

public class Person implements Actions, SpecialSkills {

    public Person() {}

    // Methods from Actions interface
    public void sleep() {
        Actions.super.sleep();
    }
    public void run() {
        Actions.super.run();
    }

    // Methods from SpecialSkills Interface
    public void computerProgram() {
        SpecialSkills.super.computerProgram();
    }
    public void driveACar() {
        SpecialSkills.super.driveACar();
    }
    /*public void driveACar() {
        // Implementation
        System.out.println("I can drive a car!");
    }*/
}
