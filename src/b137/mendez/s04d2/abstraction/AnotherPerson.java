package b137.mendez.s04d2.abstraction;

public class AnotherPerson implements Actions, SpecialSkills {
    public AnotherPerson() {}

    public void sleep() {
        Actions.super.sleep();
    }
    public void run() {
        Actions.super.run();
    }
    public void computerProgram() {
        SpecialSkills.super.computerProgram();
    }
    public void driveACar() {
        SpecialSkills.super.driveACar();
    }
}
